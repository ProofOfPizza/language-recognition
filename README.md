# Language recognition
## Installation
A simple git clone "https://gitlab.com/ProofOfPizza/language-recognition.git" will do!

## General idea
This is a program in matlab that will recognize the language of a given text.
It will first learn a language and then compare a given text to determine it's language.
It was written as part of a mathematics study course of the university of Antwerp.

## Scope
The scope is now 4 languages: Dutch, Spanish, Portuguese and English
The used alphabet is a-z. No special characters are taken into account for now.
There is a possibility to test for 2-letter combinations to improve accuracy. The impact varies according to the selected combinations as well as the amplification factor set in ```learnAll```

## Use
There are many helper functions. However it is simple to use. Only the script ```learnAll(p1,p2)``` is needed to run to make the program learn languages based on the specified and supplied texts / books. The values ```p1``` and ```p2``` are the percentages that indicate strictness for which letters to consider in the function ```relevantCols``` Play around at will but as defaults ```p1=30.0``` and ```p2=2.0``` can be used. It will save a number of ```.mat``` files:

* ```dutch.mat``` : The percentages of letters found in the dutch texts. The first column contains the total number of letters processed
* ```spanish.mat``` : The percentages of letters found in the spanish texts. The first column contains the total number of letters processed
* ```english.mat``` : The percentages of letters found in the english texts. The first column contains the total number of letters processed
* ```portuguese.mat``` : The percentages of letters found in the portuguese texts. The first column contains the total number of letters processed
* ```languages.mat``` : A matrix containing ```dutch spanish english``` and ```portuguese``` as rows
* ```reducedABC.mat``` : A sequence of letters, containing those letters where the important differences are between the languages. Based of a percentage given as a parameter into the ```relevantCols``` function.
* ```reducedLanguages.mat``` : The matrix containing the the four languages with their percentages of found letters, reduced according to the ```reducedABC```: Only the important columns.
* ```reducedSpecials``` where specials is a string of 2letter combination that may be specific for a certain language like __th__.

These variables can be loaded using for example: ```load('languages.mat')``` in matlab. Thereby learning languages does not have to run each time a new text is to be processed for recognition.

To analyse a text use the functions ```read``` and cosineSimilarity. For example: ```read(txt,reducedABC, reducedSpecials)``` will return the vector of percentages of letters found in the variable txt, and only those letters specified in both the reducedABC and reducedSpecials vectors, where the latter contains 2-letter combinations put together as a string e.g **thch** for the combinations **th** and **ch**. ```cosineSimilarity(reducedABC,v)``` will calculate how close the this vector v approximates the vectors in the given ```reducedLanguages```

To analyse (multiple texts or one single) in one go: ```analyseAll('directory','file(s)',reducedLanguages,reducedABC, reducedSpecials)```. This will produce a matrix in which each column represents a text that was analysed and contains the same info as mentioned in the above paragraph.

To analyse a text you give in at the matlab editor use ```analyseThis(yourTextVariable, reducedLanguages,reducedABC,reducedSpecials)```

## Ideas / Issues ?
Feel free to open up issues or pull requests. Have fun!
