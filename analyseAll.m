% Takes a dirName, a fileName (maybe wildcards e.g. *.*),
% an matrix of languages and an alphabet, as well as a string
% of special 2-letter combinations
% for comparing. It returns a matrix with the following rows: 
% 1: total letter count in file
% 2: cosineSimilarity to dutch
% 2: cosineSimilarity to spanish
% 2: cosineSimilarity to english
% 2: cosineSimilarity to portuguese
% example use: analyseAll('test-texts/dutch','*.txt',reducedLanguages, reducedABC, reducedSpecials)

function res = analyseAll(dirName, fileName, languages, abc, specials)
files = listDir(dirName, fileName);
M = zeros(5,length(files));
for k = 1:length(files)
    txt = readText(files(k));
    v = read(txt,abc,specials);
    M(:,k) = cosineSimilarity(languages,v);
end
M
displayResult(M(2:end,:));
res = M;
    
