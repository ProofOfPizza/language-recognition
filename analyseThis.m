% Takes a variable with text,
% an matrix of languages and an alphabet, as well as a string
% of special 2-letter combinations
% for comparing. It returns a matrix with the following rows: 
% 1: total letter count in file
% 2: cosineSimilarity to dutch
% 2: cosineSimilarity to spanish
% 2: cosineSimilarity to english
% 2: cosineSimilarity to portuguese
% example use: analyseThis(txt,reducedLanguages, reducedABC, reducedSpecials)

%NB you have to have the variables loaded f.e.:
% load ('reducedLanguages');
% load ('reducedSpecials');
% load ('reducedABC.mat');

function analyseThis(txt, languages, abc, specials)
v = read(txt,abc,specials);
M = cosineSimilarity(languages,v);
M
displayResult(M(2:end,:));
    
