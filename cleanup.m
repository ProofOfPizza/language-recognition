% Takes a vector containing a string and an alphabet. Returns a sorted
% vector with only the letters in that alphabet.

function res = cleanup(txt,abc)
normalised = normalise(txt);
ind = ismember(normalised,abc);
trimmed = normalised(ind);
res = trimmed;
