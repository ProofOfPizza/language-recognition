% Combines percentages of found letters. Takes in a matrix which has rows for each vector of percentages. 
% Each of which have a total number of words as first column.
% This function returns a weighed percentage vector.

function res = combinePercentages(M)
numRows = length(M(:,1));
for k = 1:numRows
    M(k,2:end) = M(k,2:end).*M(k,1);
end
M(numRows+1,:) = sum(M);
M(numRows+2,:) = M(numRows+1,:)/M(numRows+1,1);
M(numRows+2,1) = M(numRows+1,1);
res = M(end,:);