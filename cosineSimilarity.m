% Takes a matrix M and a vector v, and computes the cosine similarity between v and each row
% of M. Returns a vector of angles / values. (non sorted!), with the first
% column the total word count
function res = cosineSimilarity(M,v)
r = length(M(:,1)); % no of rows in the languages matrix = num of languages
w = zeros(r,1); 
tot = v(1);
v = v(2:end);
for k = 1:r
    m = M(k,:);
    w(k) = v*m' / (norm(m) * norm(v));
end
res = [tot;w];
    
    
