%Takes  a matrix and compares the rows. It will return a matrix with diff of 1-2,1-3,.. (n-1)-n 

function res = diffM (M)
r = length(M(:,1));
c = length(M(1,:));
n = r*(r-1)/2;
A = zeros(n,c);
i=1;
for j = 1:r
    for k = j:r-1
        A(i,:) = abs(M(j,:) - M(k+1,:));
        i=i+1;
    end
end
res = A;
