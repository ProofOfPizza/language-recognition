function displayResult(M)
for k = 1:length(M(1,:))
    [~, maxRow] = max(M(:,k));
    switch maxRow
        case 1
            disp('Dutch')
        case 2
            disp('Spanish')
        case 3
            disp('English')
        otherwise
            disp('Portuguese')
    end
end