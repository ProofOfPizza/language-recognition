% extract percentages of found letters from a given text. 
% The first column gives the total amount of letters that were considered.

function res = extractPercentages(txt,abc, specials)
assert(mod(length(specials),2)==0);
letters = zeros(1,length(abc)+ length(specials)/2+1); 
for k = 1:length(abc)
    letters(k+1) = sum(txt==abc(k));
end
for kk = 1:length(specials)/2
    b = specials(kk*2-1);
    c = specials(kk*2);
    occurences = find(txt(1:end-1) == b & txt(2:end) == c == 1);
    letters(length(abc)+kk+1) = length(occurences);
end
letters(1) = length(txt);
letters = round((letters./letters(1))*100,3);
letters(1) = length(txt);
res = letters;


