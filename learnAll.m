% reads the books, calculates the percentages, the important differences
% and saves these vectors. Any of these can be used elsewhere by (f.e.):
% load('dutch')
function learnAll(p1,p2) %defaults: 30.0 and 2.0
abc = ' abcdefghijklmnopqrstuvwxyz';
specials = 'thwhaooy';
% thwhaooy

dutchBooks = listDir('books/dutch','*.txt');
spanishBooks = listDir('books/spanish','*.txt');
englishBooks = listDir('books/english','*.txt');
portugueseBooks = listDir('books/portuguese','*.txt');

dutch = learnLanguage(dutchBooks,abc,specials);
save ('dutch','dutch');
spanish = learnLanguage(spanishBooks,abc,specials);
save ('spanish','spanish');
portuguese = learnLanguage(portugueseBooks,abc,specials);
save ('portuguese','portuguese');
english = learnLanguage(englishBooks,abc,specials);
save ('english','english');
languages = [dutch;spanish;english;portuguese];
save ('languages.mat','languages');

cols = relevantCols(languages,abc,p1,p2); 
reducedABC = abc(cols(1:length(abc)));
save ('reducedABC.mat','reducedABC');

specialsInd = cols(length(abc)+1:end); 
allSpecialsInd = false(1,2*length(specialsInd));
allSpecialsInd(1:2:end) = specialsInd;
allSpecialsInd(2:2:end) = specialsInd;
reducedSpecials = specials(allSpecialsInd);
save ('reducedSpecials.mat', 'reducedSpecials');


colsPad = [false,cols]; % ignore first column of total wordcount
reducedLanguages = languages(:,colsPad);
% next line aplifies the impact of the special combinations by x
reducedLanguages(:,length(reducedABC)+1:end) = 1.5*reducedLanguages(:,length(reducedABC)+1:end);
save ('reducedLanguages.mat', 'reducedLanguages');
% load ('reducedLanguages');
% load ('reducedSpecials');
% load ('reducedABC.mat');