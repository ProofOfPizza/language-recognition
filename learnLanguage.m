% learns language. Takes a list of filenames {}, a set of 
%characters and a set of special combinations and returns a weighed
% percentage vector of all of those books/ tetxts
function res = learnLanguage(books,abc, specials)
M = zeros(length(books),length(abc)+1);
for k = 1:length(books)
    txt = readText(cell2mat(books(k)));
    v = read(txt,abc,specials);
    M(k,1:length(v)) = v;
end
if length(M(:,1)) >1 
    M = combinePercentages(M);
end
res = M;
