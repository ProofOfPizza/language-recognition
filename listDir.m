%Takes a dirName and a fileName and returns the names of the files in that folder in a
%cell array. Use wildcards if required. Eg listDir('books/dutch', *.txt)'

function res = listDir (dirName,fileName)
path = fullfile(dirName,fileName);
d = dir(path);
l = length(d);
r = cell(1,l);
for k = 1:l
    str = textscan(d(k).('name'),'%s');
    r(1,k) = str;
end
res = fullfile(dirName,string(r));
    