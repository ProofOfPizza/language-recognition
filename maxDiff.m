% takes a Matrix M and a percentage p. It calcutales the max value (max) in the matrix. Then returns the same matrix with all zeros
% for each value that satifsies: M(i,j) < max*p
function res = maxDiff(M,abc,p1,p2)
M = M(:,2:end);
l = length(abc);
maxVal = max(M(:));
M1 = M(:,1:l);
M2 = M(:,l+1:end);
indices1 = M1<p1*maxVal/100;
indices2 = M2<p2*maxVal/100;
M1(indices1) = 0;
M2(indices2) = 0;
res = [M1,M2];
