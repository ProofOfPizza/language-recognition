% Takes a text, converts it to decimal ascii notation and equals out
% uppercase and loweercase
% A-Z = 65-90 ,  a-z = 97-122

function res = normalise(txt)
decString = unicode2native(txt,'utf-8');
ups = decString>=65 & decString <=90;
res = [decString(ups)+32,decString(~ups)];

