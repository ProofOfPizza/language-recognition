% Takes a Text, a set of letters and a set of special combinations
% (length 2) to check upon. Specials must contain only characters present
% in the abc.
% Does the total of reading, cleaning up and extracting percentages.
function res = read(txt,abc,specials)
format shortG;
abc = unicode2native(abc,'utf-8');
specials = unicode2native(specials,'utf-8');
clean = cleanup(txt,abc);
perc = extractPercentages(clean,abc,specials);
res = perc;
