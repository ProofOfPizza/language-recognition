function txt = readText(fileName)
textFile = fopen(fileName,'rt','n','utf-8'); 
txt = textscan(textFile,'%c','whitespace','');
fclose(textFile);
txt = txt{:,1}';