% Takes a Matrix of letters found in languages, the relevant set of chars abc, and 2 
% percentages p1 and p2. The percentage indicates 
% the strictness. Higher strictness results in less letters in the alphabet
% to be taken into account for determining the language of a text. 
% p1 refers to the letters in abc (generally around 30%) p2 refers to the specials.
% This is generally much lower because the occurences are fewer and you probably 
% DO want them taken into account.It
% returns a vector of indices indicating the relevant columns. NOTICE: it
% starts with the column for 'a', so padding may be required if the matrix
% starts with a totals column!

function res = relevantCols(M,abc,p1,p2)

d = diffM(M);
md = maxDiff(d,abc,p1,p2);
res = any(md, 1);


