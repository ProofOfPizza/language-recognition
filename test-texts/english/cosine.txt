Definition

The cosine of two non-zero vectors can be derived by using the Euclidean dot product formula:

    A ⋅ B = ‖ A ‖ ‖ B ‖ cos ⁡ θ {\displaystyle \mathbf {A} \cdot \mathbf {B} =\left\|\mathbf {A} \right\|\left\|\mathbf {B} \right\|\cos \theta } {\displaystyle \mathbf {A} \cdot \mathbf {B} =\left\|\mathbf {A} \right\|\left\|\mathbf {B} \right\|\cos \theta }

Given two vectors of attributes, A and B, the cosine similarity, cos(θ), is represented using a dot product and magnitude as

    similarity = cos ⁡ ( θ ) = A ⋅ B ‖ A ‖ ‖ B ‖ = ∑ i = 1 n A i B i ∑ i = 1 n A i 2 ∑ i = 1 n B i 2 , {\displaystyle {\text{similarity}}=\cos(\theta )={\mathbf {A} \cdot \mathbf {B} \over \|\mathbf {A} \|\|\mathbf {B} \|}={\frac {\sum \limits _{i=1}^{n}{A_{i}B_{i}}}{{\sqrt {\sum \limits _{i=1}^{n}{A_{i}^{2}}}}{\sqrt {\sum \limits _{i=1}^{n}{B_{i}^{2}}}}}},} {\displaystyle {\text{similarity}}=\cos(\theta )={\mathbf {A} \cdot \mathbf {B} \over \|\mathbf {A} \|\|\mathbf {B} \|}={\frac {\sum \limits _{i=1}^{n}{A_{i}B_{i}}}{{\sqrt {\sum \limits _{i=1}^{n}{A_{i}^{2}}}}{\sqrt {\sum \limits _{i=1}^{n}{B_{i}^{2}}}}}},}

where A i {\displaystyle A_{i}} A_{i} and B i {\displaystyle B_{i}} B_{i} are components of vector A {\displaystyle A} A and B {\displaystyle B} B respectively.

The resulting similarity ranges from −1 meaning exactly opposite, to 1 meaning exactly the same, with 0 indicating orthogonality or decorrelation, while in-between values indicate intermediate similarity or dissimilarity.

For text matching, the attribute vectors A and B are usually the term frequency vectors of the documents. Cosine similarity can be seen as a method of normalizing document length during comparison.

In the case of information retrieval, the cosine similarity of two documents will range from 0 to 1, since the term frequencies (using tf–idf weights) cannot be negative. The angle between two term frequency vectors cannot be greater than 90°.

If the attribute vectors are normalized by subtracting the vector means (e.g., A − A ¯ {\displaystyle A-{\bar {A}}} A-{\bar {A}}), the measure is called the centered cosine similarity and is equivalent to the Pearson correlation coefficient. For an example of centering, if A = [ A 1 , A 2 ] T ,  then  A ¯ = [ ( A 1 + A 2 ) 2 , ( A 1 + A 2 ) 2 ] T , {\displaystyle {\text{if}}\,A=[A_{1},A_{2}]^{T},{\text{ then }}{\bar {A}}=\left[{\frac {(A_{1}+A_{2})}{2}},{\frac {(A_{1}+A_{2})}{2}}\right]^{T},} {\displaystyle {\text{if}}\,A=[A_{1},A_{2}]^{T},{\text{ then }}{\bar {A}}=\left[{\frac {(A_{1}+A_{2})}{2}},{\frac {(A_{1}+A_{2})}{2}}\right]^{T},} so A − A ¯ = [ ( A 1 − A 2 ) 2 , ( − A 1 + A 2 ) 2 ] T . {\displaystyle A-{\bar {A}}=\left[{\frac {(A_{1}-A_{2})}{2}},{\frac {(-A_{1}+A_{2})}{2}}\right]^{T}.} {\displaystyle A-{\bar {A}}=\left[{\frac {(A_{1}-A_{2})}{2}},{\frac {(-A_{1}+A_{2})}{2}}\right]^{T}.}
Angular distance and similarity

The term "cosine similarity" is sometimes used to refer to a different definition of similarity provided below. However the most common use of "cosine similarity" is as defined above and the similarity and distance metrics defined below are referred to as "angular similarity" and "angular distance" respectively. The normalized angle between the vectors is a formal distance metric and can be calculated from the similarity score defined above[citation needed]. This angular distance metric can then be used to compute a similarity function bounded between 0 and 1, inclusive.

When the vector elements may be positive or negative:

    angular distance = cos − 1 ⁡ ( cosine similarity ) π {\displaystyle {\text{angular distance}}={\frac {\cos ^{-1}({\text{cosine similarity}})}{\pi }}} {\displaystyle {\text{angular distance}}={\frac {\cos ^{-1}({\text{cosine similarity}})}{\pi }}}

    angular similarity = 1 − angular distance {\displaystyle {\text{angular similarity}}=1-{\text{angular distance}}} {\displaystyle {\text{angular similarity}}=1-{\text{angular distance}}}

Or, if the vector elements are always positive:

    angular distance = 2 ⋅ cos − 1 ⁡ ( cosine similarity ) π {\displaystyle {\text{angular distance}}={\frac {2\cdot \cos ^{-1}({\text{cosine similarity}})}{\pi }}} {\displaystyle {\text{angular distance}}={\frac {2\cdot \cos ^{-1}({\text{cosine similarity}})}{\pi }}}

    angular similarity = 1 − angular distance {\displaystyle {\text{angular similarity}}=1-{\text{angular distance}}} {\displaystyle {\text{angular similarity}}=1-{\text{angular distance}}}

Although the term "cosine similarity" has been used for this angular distance, the term is used as the cosine of the angle only as a convenient mechanism for calculating the angle itself and is no part of the meaning. The advantage of the angular similarity coefficient is that, when used as a difference coefficient (by subtracting it from 1) the resulting function is a proper distance metric, which is not the case for the first meaning. However, for most uses this is not an important property. For any use where only the relative ordering of similarity or distance within a set of vectors is important, then which function is used is immaterial as the resulting order will be unaffected by the choice.
Confusion with "Tanimoto" coefficient

The cosine similarity may be easily confused with the Tanimoto metric, a specialised similarity coefficient with a similar algebraic form:

    T ( A , B ) = A ⋅ B ‖ A ‖ 2 + ‖ B ‖ 2 − A ⋅ B {\displaystyle T(A,B)={A\cdot B \over \|A\|^{2}+\|B\|^{2}-A\cdot B}} T(A,B)={A\cdot B \over \|A\|^{2}+\|B\|^{2}-A\cdot B}

In fact, this algebraic form was first defined by Tanimoto as a mechanism for calculating the Jaccard coefficient in the case where the sets being compared are represented as bit vectors. While the formula extends to vectors in general, it has quite different properties from cosine similarity and bears little relation other than its superficial appearance.
Otsuka-Ochiai coefficient

In biology, there is a similar concept known as the Otsuka-Ochiai coefficient named after Yanosuke Otsuka (also spelled as Ōtsuka, Ootsuka or Otuka,[3] Japanese: 大塚 弥之助)[4] and Akira Ochiai (Japanese: 落合 明),[5] also known as the Ochiai-Barkman[6] or Ochiai coefficient,[7] which can be represented as:

    K = | A ∩ B | | A | × | B | {\displaystyle K={\frac {|A\cap B|}{\sqrt {|A|\times |B|}}}} {\displaystyle K={\frac {|A\cap B|}{\sqrt {|A|\times |B|}}}}

Here, A {\displaystyle A} A and B {\displaystyle B} B are sets, and | A | {\displaystyle |A|} |A| is the number of elements in A {\displaystyle A} A. If sets are represented as bit vectors, the Otsuka-Ochiai coefficient can be seen to be the same as the cosine similarity.

In a recent book,[8] the coefficient is misattributed to another Japanese researcher with the family name Otsuka. The confusion arises because in 1957 Akira Ochiai attributes the coefficient to Otsuka by citing an article by Ikuso Hamai (Japanese: 浜井 生三),[9] who in turn cites the original 1936 article by Yanosuke Otsuka.[4]
Properties

Cosine similarity is related to Euclidean distance as follows. Denote Euclidean distance by the usual ‖ A − B ‖ {\displaystyle \|A-B\|} \|A-B\|, and observe that

    ‖ A − B ‖ 2 = ( A − B ) ⊤ ( A − B ) = ‖ A ‖ 2 + ‖ B ‖ 2 − 2 A ⊤ B {\displaystyle \|A-B\|^{2}=(A-B)^{\top }(A-B)=\|A\|^{2}+\|B\|^{2}-2A^{\top }B} \|A-B\|^{2}=(A-B)^{\top }(A-B)=\|A\|^{2}+\|B\|^{2}-2A^{\top }B

by expansion. When A and B are normalized to unit length, ‖ A ‖ 2 = ‖ B ‖ 2 = 1 {\displaystyle \|A\|^{2}=\|B\|^{2}=1} \|A\|^{2}=\|B\|^{2}=1 so this expression is equal to

    2 ( 1 − cos ⁡ ( A , B ) ) . {\displaystyle 2(1-\cos(A,B)).} {\displaystyle 2(1-\cos(A,B)).}

The Euclidean distance is called the chord distance (because it is the length of the chord on the unit circle) and it is the Euclidean distance between the vectors which were normalized to unit sum of squared values within them.

Null distribution: For data which can be negative as well as positive, the null distribution for cosine similarity is the distribution of the dot product of two independent random unit vectors. This distribution has a mean of zero and a variance of 1 / n {\displaystyle 1/n} 1/n (where n {\displaystyle n} n is the number of dimensions), and although the distribution is bounded between -1 and +1, as n {\displaystyle n} n grows large the distribution is increasingly well-approximated by the normal distribution.[10][11] Other types of data such as bitstreams, which only take the values 0 or 1, the null distribution takes a different form and may have a nonzero mean.[12]
Soft cosine measure

A soft cosine or ("soft" similarity) between two vectors considers similarities between pairs of features.[13] The traditional cosine similarity considers the vector space model (VSM) features as independent or completely different, while the soft cosine measure proposes considering the similarity of features in VSM, which help generalize the concept of cosine (and soft cosine) as well as the idea of (soft) similarity.

For example, in the field of natural language processing (NLP) the similarity among features is quite intuitive. Features such as words, n-grams, or syntactic n-grams[14] can be quite similar, though formally they are considered as different features in the VSM. For example, words “play” and “game” are different words and thus mapped to different points in VSM; yet they are semantically related. In case of n-grams or syntactic n-grams, Levenshtein distance can be applied (in fact, Levenshtein distance can be applied to words as well).

For calculating soft cosine, the matrix s is used to indicate similarity between features. It can be calculated through Levenshtein distance, WordNet similarity, or other similarity measures. Then we just multiply by this matrix.

Given two N-dimension vectors a {\displaystyle a} a and b {\displaystyle b} b, the soft cosine similarity is calculated as follows:

    s o f t _ c o s i n e 1 ⁡ ( a , b ) = ∑ i , j N s i j a i b j ∑ i , j N s i j a i a j ∑ i , j N s i j b i b j , {\displaystyle {\begin{aligned}\operatorname {soft\_cosine} _{1}(a,b)={\frac {\sum \nolimits _{i,j}^{N}s_{ij}a_{i}b_{j}}{{\sqrt {\sum \nolimits _{i,j}^{N}s_{ij}a_{i}a_{j}}}{\sqrt {\sum \nolimits _{i,j}^{N}s_{ij}b_{i}b_{j}}}}},\end{aligned}}} {\begin{aligned}\operatorname {soft\_cosine} _{1}(a,b)={\frac {\sum \nolimits _{i,j}^{N}s_{ij}a_{i}b_{j}}{{\sqrt {\sum \nolimits _{i,j}^{N}s_{ij}a_{i}a_{j}}}{\sqrt {\sum \nolimits _{i,j}^{N}s_{ij}b_{i}b_{j}}}}},\end{aligned}}

where sij = similarity(featurei, featurej).

If there is no similarity between features (sii = 1, sij = 0 for i ≠ j), the given equation is equivalent to the conventional cosine similarity formula.

The time complexity of this measure is quadratic, which makes it perfectly applicable to real-world tasks but it can be reduced to subquadratic.[15]
See also

    Sørensen similarity index
    Hamming distance
    Correlation
    Dice's coefficient
    Jaccard index
    SimRank
    Information retrieval

References

Singhal, Amit (2001). "Modern Information Retrieval: A Brief Overview". Bulletin of the IEEE Computer Society Technical Committee on Data Engineering 24 (4): 35–43.
P.-N. Tan, M. Steinbach & V. Kumar, Introduction to Data Mining, Addison-Wesley (2005), ISBN 0-321-32136-7, chapter 8; page 500.
Omori, Masae (2004). "Geological idea of Yanosuke Otuka, who built the foundation of neotectonics (geoscientist)". Earth Science. 58 (4): 256–259. doi:10.15080/agcjchikyukagaku.58.4_256.
Otsuka, Yanosuke (1936). "The faunal character of the Japanese Pleistocene marine Mollusca, as evidence of the climate having become colder during the Pleistocene in Japan". Bulletin of the Biogeographical Society of Japan. 6 (16): 165–170.
Ochiai, Akira (1957). "Zoogeographical studies on the soleoid fishes found in Japan and its neighhouring regions-II". Bulletin of the Japanese Society of Scientific Fisheries. 22 (9): 526–530. doi:10.2331/suisan.22.526.
Barkman, Jan J. (1958). Phytosociology and Ecology of Cryptogamic Epiphytes: Including a Taxonomic Survey and Description of Their Vegetation Units in Europe. Assen: Van Gorcum.
H. Charles Romesburg (1984). Cluster Analysis for Researchers. Belmont, California: Lifetime Learning Publications. p. 149.
Howarth, Richard J. (2017). Dictionary of Mathematical Geosciences: With Historical Notes. Cham, Switzerland: Springer. p. 421. doi:10.1007/978-3-319-57315-1. ISBN 978-3-319-57314-4.
Hamai, Ikuso (1955). "Stratification of community by means of "community coefficient" (continued)". Japanese Journal of Ecology. 5 (1): 41–45. doi:10.18960/seitai.5.1_41.
Spruill, Marcus C. (2007). "Asymptotic distribution of coordinates on high dimensional spheres". Electronic Communications in Probability. 12: 234–247. doi:10.1214/ECP.v12-1294.
"CrossValidated: Distribution of dot products between two random unit vectors in RD"
Graham L. Giller (2012). "The Statistical Properties of Random Bitstreams and the Sampling Distribution of Cosine Similarity". Giller Investments Research Notes (20121024/1). doi:10.2139/ssrn.2167044.
Sidorov, Grigori; Gelbukh, Alexander; Gómez-Adorno, Helena; Pinto, David (29 September 2014). "Soft Similarity and Soft Cosine Measure: Similarity of Features in Vector Space Model". Computación y Sistemas. 18 (3): 491–504. doi:10.13053/CyS-18-3-2043. Retrieved 7 October 2014.
Sidorov, Grigori; Velasquez, Francisco; Stamatatos, Efstathios; Gelbukh, Alexander; Chanona-Hernández, Liliana (2013). Advances in Computational Intelligence. Lecture Notes in Computer Science. 7630. LNAI 7630. pp. 1–11. doi:10.1007/978-3-642-37798-3_1. ISBN 978-3-642-37798-3.

    Novotný, Vít (2018). Implementation Notes for the Soft Cosine Measure. The 27th ACM International Conference on Information and Knowledge Management. Torun, Italy: Association for Computing Machinery. pp. 1639–1642. arXiv:1808.09407. doi:10.1145/3269206.3269317. ISBN 978-1-4503-6014-2.

External links

    Weighted cosine measure
    A tutorial on cosine similarity using Python
